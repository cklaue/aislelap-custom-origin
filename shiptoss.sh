#!/bin/bash
mvn clean package clean package -DskipTests \
&& rm /Users/chris/phdata/streamsets-datacollector-3.3.1/user-libs/aisle-1.0-SNAPSHOT.tar.gz \
&& rm -rf /Users/chris/phdata/streamsets-datacollector-3.3.1/user-libs/aisle* \
&& cp target/aisle-1.0-SNAPSHOT.tar.gz /Users/chris/phdata/streamsets-datacollector-3.3.1/user-libs/ \
&& tar xvfz /Users/chris/phdata/streamsets-datacollector-3.3.1/user-libs/aisle-1.0-SNAPSHOT.tar.gz -C /Users/chris/phdata/streamsets-datacollector-3.3.1/user-libs
