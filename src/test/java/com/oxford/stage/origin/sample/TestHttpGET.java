/**
 * Copyright 2015 StreamSets Inc.
 *
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oxford.stage.origin.sample;


import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;


public class TestHttpGET {
  String tdid = "1223";
  String endTime = "2017-11-30%2010:00:00";
  String user_key = "c553480fda09feff6e13dbe88843b956";
  String startTime = "2017-11-29%2010:00:00";
  String maxPhones = "10";
  String connectedOnly = "true";
  //String url = "https://api.aislelabs.com/api/rest/metrics/visitlist.json?tdid=1223&user_key=&startTime=2017-11-29%2010:00:00&endTime=2017-11-30%2010:00:00&maxPhones=10&connectedOnly=true";
  String url = String.format("https://api.aislelabs.com/api/rest/metrics/visitlist.json?" +
                  "tdid=%s&user_key=%s&startTime=%s" +
                  "&endTime=%s"+
                  "&maxPhones=%s"+
                  "&connectedOnly=%s", tdid, user_key, startTime, endTime, maxPhones, connectedOnly);

  @Test
  public void testOrigin() throws Exception {
    try {
      System.out.println(url);
      //safeUrl = URLEncoder.encode(url, "UTF-8");
      //SSLContextBuilder builder = new SSLContextBuilder();
      //HttpClient client = HttpClientBuilder.create().build();

      CloseableHttpClient client = HttpClients.createDefault();
      HttpGet httpGet = new HttpGet(url);
      httpGet.setHeader(HttpHeaders.ACCEPT, "application/xml");
//      HttpParams params = new BasicHttpParams();
//      params.setParameter("tdid", "1223");
//      params.setParameter("user_key", "c553480fda09feff6e13dbe88843b956");
//      params.setParameter("startTime", "2017-11-29%2010:00:00");
//      params.setParameter("endTime", "2017-11-30%2010:00:00");
//      params.setParameter("maxPhones", "5");
//      params.setParameter("connectedOnly", "true");
//      httpGet.setParams(params);

      //HttpResponse response1 = client.execute(httpGet);
      CloseableHttpResponse response1 = client.execute(httpGet);
      System.out.println("STATUSLINE");
      System.out.println(response1.getStatusLine());
      HttpEntity entity1 = response1.getEntity();
      // do something useful with the response body
      String responseRaw = EntityUtils.toString(entity1);
      System.out.println("RESPONSE");
      System.out.println(responseRaw);



      EntityUtils.consume(entity1);
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    } catch (ClientProtocolException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


}
