/**
 * Copyright 2015 StreamSets Inc.
 *
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oxford.stage.origin.sample;


import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class TestDateTime {
  public static DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
  public static DateFormat queryFormat = new SimpleDateFormat("yyyy-MM-dd%20HH:00:00");

  String startTime = "2017-11-29%2010:00:00";
  String endTime = "2017-11-30%2010:00:00";


  @Test
  public void testIt() throws Exception {

    Date offset = format.parse("2017-11-29 08:00:00");
    System.out.println(offset.toString());
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(offset);
    calendar.add(Calendar.HOUR, 1);
    Date nextOffset = calendar.getTime();
    System.out.println(nextOffset.toString());

    String queryDateStart = queryFormat.format(offset);
    String queryDateEnd = queryFormat.format(nextOffset);

    System.out.println(queryDateStart);
    System.out.println(queryDateEnd);
    Date d = new Date();
    System.out.println(d.after(offset));



  }


}
