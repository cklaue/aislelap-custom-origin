/**
 * Copyright 2015 StreamSets Inc.
 *
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oxford.stage.origin.sample;

import com.streamsets.pipeline.api.BatchMaker;
import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.api.StageException;
import com.streamsets.pipeline.api.base.BaseSource;
import com.streamsets.pipeline.config.JsonMode;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;

import com.streamsets.pipeline.lib.parser.DataParserFactory;
import com.streamsets.pipeline.lib.parser.DataParserFactoryBuilder;
import com.streamsets.pipeline.lib.parser.DataParserFormat;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public abstract class SampleSource extends BaseSource {

  public static DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
  public static DateFormat queryFormat = new SimpleDateFormat("yyyy-MM-dd%20HH:00:00");

  /**
   * Gives access to the UI configuration of the stage provided by the {@link SampleDSource} class.
   */
//  public abstract boolean getConnectedOnly();
  public abstract int getMaxPhones();
//  public abstract String getUser_key();
  public abstract int getInterval();
  public abstract String getStartTime();
//  public abstract List getTTIDs();
  public abstract int getRTimeout();
  public abstract Map getlocUserKeys();
  private static final Logger LOG = LoggerFactory.getLogger(SampleSource.class);


  @Override
  protected List<ConfigIssue> init() {
    // Validate configuration values and open any required resources.
    List<ConfigIssue> issues = super.init();

//    if (getConfig().equals("invalidValue")) {
//      issues.add(
//          getContext().createConfigIssue(
//              Groups.AisleLabs.name(), "config", Errors.SAMPLE_00, "Here's what's wrong..."
//          )
//      );
//    }
    if (getRTimeout() < 20) {
      issues.add(
          getContext().createConfigIssue(
              Groups.AisleLabs.name(), "config", Errors.AISLE_rtimeout, ""
          )
      );
    }
    //TODO: Inlcude rest of the errors
    // If issues is not empty, the UI will inform the user of each configuration issue in the list.
    return issues;
  }

  /** {@inheritDoc} */
  @Override
  public void destroy() {
    // Clean up any open resources.
    super.destroy();
  }

  /** {@inheritDoc} */
  @Override
  public String produce(String lastSourceOffset, int maxBatchSize, BatchMaker batchMaker) throws StageException {
    Date offset = null;
    try {
      offset = format.parse(getStartTime());
    } catch (ParseException e) {
      e.printStackTrace();
    }
    Date nextOffset = null;
    Date now = new Date();
    try {

      if (lastSourceOffset != null) {
        LOG.error("Restored old offset: " + lastSourceOffset);
        offset = format.parse(lastSourceOffset);
      }

      Calendar calendar = Calendar.getInstance();
      calendar.setTime(offset);
      calendar.add(Calendar.HOUR, getInterval());
      nextOffset = calendar.getTime();

      if (nextOffset.after(now)){
          nextOffset = now;
        }

      Map tidsUserAPIkeys = getlocUserKeys();
      String endTime = queryFormat.format(nextOffset);//"2017-11-30%2010:00:00";
      String startTime = queryFormat.format(offset);//"2017-11-29%2010:00:00";
      String maxPhones = String.valueOf(getMaxPhones());
      String[] connectedOnlyArray = new String[]{"true","false"};
      //String connectedOnly = "true";  // For now we just collect connected data
      for (String connectedOnly : connectedOnlyArray) {
        for (Object iterttid : tidsUserAPIkeys.keySet()) {
          String ttid = (String) iterttid;
          Object user_key = tidsUserAPIkeys.get(ttid);
          //Sample URL "https://api.aislelabs.com/api/rest/metrics/visitlist.json?tdid=1223&user_key=&startTime=2017-11-29%2010:00:00&endTime=2017-11-30%2010:00:00&maxPhones=10&connectedOnly=true";
          String url = String.format("https://api.aislelabs.com/api/rest/metrics/visitlist.json?" +
                  "tdid=%s&user_key=%s&startTime=%s" +
                  "&endTime=%s" +
                  "&maxPhones=%s" +
                  "&connectedOnly=%s", ttid, user_key, startTime, endTime, maxPhones, connectedOnly);
          String response = makeRequest(url);
          DataParserFactoryBuilder dataParserFactoryBuilder =
                  new DataParserFactoryBuilder(getContext(), DataParserFormat.JSON);
          DataParserFactory factory = dataParserFactoryBuilder
                  .setMaxDataLen(-1)
                  .setMode(JsonMode.MULTIPLE_OBJECTS)
                  .build();
          try (com.streamsets.pipeline.lib.parser.DataParser parser = factory.getParser(connectedOnly, response)) {
            Record record;
            while ((record = parser.parse()) != null) {
              batchMaker.addRecord(record);
            }

          }
        }
        Thread.sleep((getRTimeout()+1) *  1000); //sleep for getInterval minutes

      }
      return String.valueOf(format.format(nextOffset));
    //TODO: Trigger SS errors here as well.
    } catch (ParseException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (KeyStoreException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return String.valueOf(format.format(offset));

  }



  /**
   * Helper method to construct an HTTP request and fetch a response.
   *
   * @param url the target url to fetch.
   * @throws StageException if an unhandled error is encountered
   */
  private String makeRequest(String url) throws StageException, IOException, KeyStoreException, NoSuchAlgorithmException {

    try {
      LOG.error("REST API CALL:   " + url);
      CloseableHttpClient client = HttpClients.createDefault();
      HttpGet httpGet = new HttpGet(url);
      httpGet.setHeader(HttpHeaders.ACCEPT, "application/xml");

      CloseableHttpResponse response1 = client.execute(httpGet);
      //System.out.println("STATUSLINE");
      //System.out.println(response1.getStatusLine());
      HttpEntity entity1 = response1.getEntity();
      String responseRaw = EntityUtils.toString(entity1);

      EntityUtils.consume(entity1);
      return responseRaw;

    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    } catch (ClientProtocolException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return "";
  }
}
