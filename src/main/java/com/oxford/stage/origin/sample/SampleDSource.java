/**
 * Copyright 2015 StreamSets Inc.
 *
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oxford.stage.origin.sample;

import com.google.common.collect.ImmutableMap;
import com.streamsets.pipeline.api.ConfigDef;
import com.streamsets.pipeline.api.ConfigGroups;
import com.streamsets.pipeline.api.ExecutionMode;
import com.streamsets.pipeline.api.GenerateResourceBundle;
import com.streamsets.pipeline.api.StageDef;
import com.streamsets.pipeline.api.ValueChooserModel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@StageDef(
    version = 1,
    label = "Aisle Origin",
    description = "",
    icon = "default.png",
    execution = ExecutionMode.STANDALONE,
    recordsByRef = true,
    onlineHelpRefUrl = "",
    resetOffset = true
)
@ConfigGroups(value = Groups.class)
@GenerateResourceBundle
public class SampleDSource extends SampleSource {

  @ConfigDef(
      required = true,
      type = ConfigDef.Type.STRING,
      defaultValue = "2017-11-29 08:00:00",
      label = "Start Time",
      description = "The start end time parameters refer to start of the visit. Time format must be yyyy-MM-dd HH:00:00.",
      displayPosition = 10,
      group = "AisleLabs"
  )
  public String startTime = "2017-11-29 08:00:00";

  @ConfigDef(
          required = true,
          type = ConfigDef.Type.NUMBER,
          defaultValue = "1",
          label = "Interval",
          description = "How long in hours after the Start Time should visits be queried in each request?",
          displayPosition = 20,
          group = "AisleLabs"
  )
  public int interval = 3;

//  @ConfigDef(
//          required = true,
//          type = ConfigDef.Type.STRING,
//          defaultValue = "c553480fda09feff6e13dbe88843b956&",
//          label = "User Key",
//          description = "Your API key",
//          displayPosition = 30,
//          group = "AisleLabs"
//  )
//  public String user_key = "c553480fda09feff6e13dbe88843b956&";

  @ConfigDef(
          required = true,
          type = ConfigDef.Type.NUMBER,
          defaultValue = "100",
          label = "Phones",
          description = "The maximum number of visitor infos to return. Max value is 100,000 visitor info in a single API call",
          displayPosition = 40,
          group = "AisleLabs"
  )
  public int maxPhones = 100;

//  @ConfigDef(
//          required = true,
//          type = ConfigDef.Type.BOOLEAN,
//          defaultValue = "false",
//          label = "Only Connected",
//          description = "If true, filter for visits that have connected to social wifi and include the following additional fields for each visit response object:\n" +
//                  "Login_Type, Name, Email_Address, Phone_Number, Age_Range, Gender, Date_Of_Birth, Current_City, Hometown, Postal_Code, Country_Of_Residence, FB_Likes.",
//          displayPosition = 50,
//          group = "AisleLabs"
//  )
//  public boolean connectedOnly = false;

    @ConfigDef(
          required = true,
          type = ConfigDef.Type.NUMBER,
          defaultValue = "20",
          label = "Request Timeout",
          description = "How many seconds to wait between AisleLabs API calls. Must be more than 20 seconds.",
          displayPosition = 50,
          group = "AisleLabs"
  )
  public int rTimeout = 20;


//  @ConfigDef(
//          label = "TTIDS",
//          type = ConfigDef.Type.LIST,
//          required = true,
//          description = "Specify TTIDs which should be queried for each batch.",
//          defaultValue = "[\"1223\", \"1325\", \"608\", \"682\", \"1324\", \"704\", \"1117\", \"1962\"]",
//          displayPosition = 60,
//          group = "AisleLabs"
//
//  )
//  public List<String> TTIDs = Arrays.asList("1223", "1325", "608", "682", "1324", "704", "1117", "1962");

  @ConfigDef(
          label = "Location User API Keys",
          type = ConfigDef.Type.MAP,
          required = true,
          description = "Specify TTIDs and the associated User API Key.",
          defaultValue = "{ \"1223\": \"c553480fda09feff6e13dbe88843b956\", " +
                            "\"1325\": \"c553480fda09feff6e13dbe88843b956\", " +
                            "\"608\": \"c553480fda09feff6e13dbe88843b956\", " +
                            "\"682\": \"c553480fda09feff6e13dbe88843b956\", " +
                            "\"1324\": \"c553480fda09feff6e13dbe88843b956\", " +
                            "\"704\": \"c553480fda09feff6e13dbe88843b956\", " +
                            "\"1117\": \"c553480fda09feff6e13dbe88843b956\", " +
                            "\"1962\": \"c553480fda09feff6e13dbe88843b956\", " +
                            "\"1309\": \"c553480fda09feff6e13dbe88843b956\"}",
          displayPosition = 70,
          group = "AisleLabs"

  )
  public Map<String, String> locUserKeys = ImmutableMap.<String, String>builder()
          .put("1223", "c553480fda09feff6e13dbe88843b956")
          .put("1325", "c553480fda09feff6e13dbe88843b956")
          .put("608", "c553480fda09feff6e13dbe88843b956")
          .put("682", "c553480fda09feff6e13dbe88843b956")
          .put("1324", "c553480fda09feff6e13dbe88843b956")
          .put("704", "c553480fda09feff6e13dbe88843b956")
          .put("1117", "c553480fda09feff6e13dbe88843b956")
          .put("1962", "c553480fda09feff6e13dbe88843b956")
          .put("1309", "c553480fda09feff6e13dbe88843b956")
          .build();


  /** {@inheritDoc} */
//  public boolean getConnectedOnly() {
//    return connectedOnly;
//  }
  public int getMaxPhones() {
    return maxPhones;
  }
//  public String getUser_key() {
//    return user_key;
//  }
  public int getInterval() {
    return interval;
  }
  public String getStartTime() {
    return startTime;
  }
//  public List getTTIDs() {
//    return TTIDs;
//  }
  public int getRTimeout() {
    return rTimeout;
  }
  public Map getlocUserKeys() {
    return locUserKeys;
  }

}
